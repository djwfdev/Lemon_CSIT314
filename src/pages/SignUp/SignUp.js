import React from 'react';
import '../../styles/App.css';
import '../../styles/Button.css';
import { Link } from 'react-router-dom';

const SignUp = () => {
  return (
    <>
      <div className='sign-up'>
        <Link
            to='/sign-up-driver'
            className='btn-card'
          >
            Sign Up as Driver
        </Link>
        <Link
            to='/sign-up-ap'
            className='btn-card'
          >
            Sign Up as AP
        </Link>
      </div>
    </>
  );
}

export { SignUp as default };