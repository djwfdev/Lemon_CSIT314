import React from 'react';
import '../../styles/App.css';
import '../../styles/Button.css'; 
import { Link } from 'react-router-dom';

const Login = () => {
  return (
    <>
      <div className='login'>
        <Link
            to='/log-in-driver'
            className='btn-card'
          >
            Log in as Driver
        </Link>
        <Link
            to='/log-in-ap'
            className='btn-card'
          >
            Log in as AP
        </Link>
      </div>
    </>
  );
}

export { Login as default };