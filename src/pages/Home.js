import React from 'react';
import '../styles/App.css';

const Home = () => {
  return (
    <>
      <h1 className='home'>
        <img src={require('../images/logo_lrg.png')} alt='logo' />
      </h1>
    </>
  );
}

export { Home as default };