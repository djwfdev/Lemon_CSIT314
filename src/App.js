import React from 'react';
import Navbar from './components/Navbar';
import './styles/App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import Home from './pages/Home';
import About from './pages/About';
import ContactUs from './pages/ContactUs';
import SignUp from './pages/SignUp/SignUp';
import SignUpDriver from './pages/SignUp/SignUpDriver';
import SignUpAP from './pages/SignUp/SignUpAP';
import Login from './pages/Login/Login';
import LoginDriver from './pages/Login/LoginDriver';
import LoginAP from './pages/Login/LoginAP';

const App = () => {
  return (
    <Router>
      <Navbar />
      <Routes>
        <Route path='/' exact element={<Home/>} />
        <Route path='/about' element={<About/>} />
        <Route path='/contact-us' element={<ContactUs/>} />
        <Route path='/login' element={<Login/>} />
        <Route path='/log-in-driver' element={<LoginDriver/>} />
        <Route path='/log-in-ap' element={<LoginAP/>} />
        <Route path='/sign-up' element={<SignUp/>} />
        <Route path='/sign-up-driver' element={<SignUpDriver/>} />
        <Route path='/sign-up-ap' element={<SignUpAP/>} />
      </Routes>
    </Router>
  );
}

export default App;
